from pymongo import MongoClient
import re
import json

class MongoConnection:
    
    def __init__(self, client, db, collection_source, collection_target):
        """
        Define the DB target addresses
        """
        self.client = MongoClient(client)
        self.db = self.client[db]
        self.collectionSource = self.db[collection_source]
        self.collectionTarget = self.db[collection_target]
 
    def getConntactLink(self):
        documents = self.collectionSource.find({"classified": { '$exists': False }}, {"Link": 1}).limit(1000)
        return list(documents)

    def write(self, _id, classified_news):
        # Saving the model output to MongoDB
        toJSON = json.dumps(classified_news)
        if self.collectionSource.find({'_id': _id}).limit(1).count() == 0:
            self.collectionSource.insert_one(json.loads(toJSON))
    
    def update(self, _id, email, name, company, position):
        myquery = { "_id": _id }
        newvalues = { "$set": { "Email": email, "Name": name, "Company":company, "Position":position } }
        self.collectionSource.update_one(myquery, newvalues)

    # exist Id document source
    def notExistDocument(self, _id):
        if self.collectionSource.find({'_id': _id}).limit(1).count() == 0:
            return True
        
    def flag_classified(article):
        myquery = { "_id": article["_id"] }
        newvalues = { "$set": { "_id": article._id, "classified": True} }
        self.collectionSource.update_one(article.id)
    
    def write_one_db(self, _id, classification):
        #flag_classified(article)
        # myquery = { "_id": _id }
        # newvalues = { "$set": { "_id": _id, "classified": True} }
        # self.collectionSource.update_one(myquery, newvalues)
        print("I will insert the document")
        self.collectionTarget.insert_one(classification)
    
    def write_as_unclassified(self, _id, error):
        myquery = { "_id": _id }
        newvalues = { "$set": { "_id": _id, "classified": False, "error": error} }
        self.collectionSource.update_one(myquery, newvalues)
